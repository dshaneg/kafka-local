# kafka-local

Use docker-compose, and [Kafdrop](https://github.com/obsidiandynamics/kafdrop) from [obsidiandynamics](https://github.com/obsidiandynamics) to create a single node kafka cluster running on your local machine. The docker-compose.yml file is lifted straight from Obsidiandynamics' repo.

## Setup

You'll need to have `docker`, `docker-compose` and `make` installed on your machine.

## Usage

### Managing the local cluster

To spin up your single-node kafka cluster, along with the kafdrop dashboard:

```sh
make up
```

To access the kafdrop site, point your browser to [localhost:9000](http://localhost:9000).

To spin down:

```sh
make down
```

There are a couple of extra make rules to show you the logs in the kafka and kafdrop containers.

```sh
make log-kafka
make log-kafdrop
```

I also added a make rule for `docker ps`, because my fingers wanted to start every command with `make`.

```sh
make ps
```

### Using your kafka instance

You will need to specify `localhost:9092` as your kafka host when connecting to your cluster programmatically.
