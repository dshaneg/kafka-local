.PHONY: up down ps log-kafka log-kafdrop

up:
	docker-compose up -d

down:
	docker-compose down

ps:
	docker ps

log-kafka:
	docker logs kafka-local_kafka_1

log-kafdrop:
	docker logs kafka-local_kafdrop_1
